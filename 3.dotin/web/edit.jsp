<%--
  Created by IntelliJ IDEA.
  User: Hi
  Date: 8/3/2020
  Time: 1:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        input[type=text], select {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        /* Style the submit button */
        input[type=submit] {
            width: 100%;
            background-color: #4CAF50;
            color: white;
            padding: 14px 20px;
            margin: 8px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        /* Add a background color to the submit button on mouse-over */
        input[type=submit]:hover {
            background-color: #45a049;
        }
    </style>
    <title>Title</title>
</head>
<script>
</script>

<body>
PERSONCUSTOMER
<form action="/update.do">
    <input  type="text"  name="id" id="id"  value="${requestScope.myid}" placeholder="<%out.print(request.getParameter("id"));%>">
    <input type="text" name="name" placeholder="NAME">
    <input type="text" name="lastName" placeholder="LAST NAME">
    <input type="text" name="identityNumber" placeholder="IDENTITY NUMBER">
    <input type="text" name="nameOfFather" placeholder="NAME OF FATHER">
    <input type="text" name="dateOfBirth" placeholder="DATE OF BIRTH">
    <input type="submit" value="update">
</form>
</br>
COMPANYCUSTOMER
<form action="/updatecompanycustomer.do">
    <input type="text" name="companyId" placeholder="<%out.print(request.getParameter("id2"));%>" value="${requestScope.myid1}">
    <input type="text" name="companyName" placeholder="COMPANY NAME">
    <input type="text" name="economicId" placeholder="ECONOMIC ID">
    <input type="text" name="dateOfRegistering" placeholder="DATE OF REGISTERING">
    <input type="submit" value="update">
</form>

</body>
</html>

package controller;

import model.entity.LegalCustomer;
import common.service.LegalCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/updatecompanycustomer.do")
public class UpdateCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LegalCustomerService legalCustomerService = new LegalCustomerService();
        LegalCustomer legalCustomer = new LegalCustomer(Integer.parseInt(req.getParameter("companyId")), req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            System.out.println("in service update:"+"id:"+legalCustomer.getId()+"company name"+legalCustomer.getCompanyName()+"economicid"+legalCustomer.getEconomicId());
            legalCustomerService.update(legalCustomer);
            resp.sendRedirect("/findallcompanycustomer.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

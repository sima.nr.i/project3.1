package controller;

import model.entity.RealCustomer;
import common.service.RealCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/1/2020.
 */
@WebServlet("/update.do")
public class Update extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RealCustomerService realCustomerService = new RealCustomerService();
        RealCustomer realCustomer = new RealCustomer(Integer.parseInt(req.getParameter("id")), req.getParameter("name"), req.getParameter("lastName")
                , req.getParameter("identityNumber"), req.getParameter("nameOfFather"), req.getParameter("dateOfBirth"));
        try {
            realCustomerService.update(realCustomer);
            resp.sendRedirect("/findAll.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

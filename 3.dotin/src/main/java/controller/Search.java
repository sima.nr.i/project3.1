package controller;

import model.entity.RealCustomer;
import common.service.RealCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/16/2020.
 */
@WebServlet("/search.do")
public class Search extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            RealCustomer realCustomer;
            if (req.getParameter("id")=="")
            {
                realCustomer =new RealCustomer(-1,req.getParameter("name"),
                        req.getParameter("lastname"),req.getParameter("identitynumber"));

            }
            else
            realCustomer =new RealCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("name"),
                    req.getParameter("lastname"),req.getParameter("identitynumber"));
            RealCustomerService realCustomerService = new RealCustomerService();
            req.setAttribute("list", realCustomerService.search(realCustomer));
            req.getRequestDispatcher("/managing_personcustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

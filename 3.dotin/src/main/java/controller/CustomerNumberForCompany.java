package controller;

import common.service.LegalCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/11/2020.
 */
@WebServlet("/showingcustomernumberforcompany.do")
public class CustomerNumberForCompany extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            req.setAttribute("customernumberforcompany", legalCustomerService.showCustomerNumber((Integer) req.getAttribute("id2")));
            req.getRequestDispatcher("/showingcustomernumberforcompany.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

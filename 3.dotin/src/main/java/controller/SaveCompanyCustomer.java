package controller;

import model.entity.LegalCustomer;
import common.service.LegalCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/3/2020.
 */
@WebServlet("/savecompanycustomer.do")
public class SaveCompanyCustomer extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LegalCustomer legalCustomer = new LegalCustomer(req.getParameter("companyName"), req.getParameter("economicId")
                , req.getParameter("dateOfRegistering"));
        try {
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            legalCustomerService.save(legalCustomer);
            req.setAttribute("id2",legalCustomer.getId());
            req.getRequestDispatcher("/showingcustomernumberforcompany.do").forward(req,resp);
//          resp.sendRedirect("/showingcustomernumberforcompany.do");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

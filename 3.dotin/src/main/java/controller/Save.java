package controller;

import model.entity.RealCustomer;
import common.service.RealCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/1/2020.
 */
@WebServlet("/save.do")
public class Save extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RealCustomer realCustomer = new RealCustomer(req.getParameter("name"), req.getParameter("lastName")
                , req.getParameter("identityNumber"), req.getParameter("nameOfFather"), req.getParameter("dateOfBirth"));
        try {
            RealCustomerService realCustomerService=new RealCustomerService();
            realCustomerService.save(realCustomer);
            req.setAttribute("id",realCustomer.getId());
            req.getRequestDispatcher("/showingcustomernumber.do").forward(req,resp);
           // resp.sendRedirect("/showingcustomernumber.do");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

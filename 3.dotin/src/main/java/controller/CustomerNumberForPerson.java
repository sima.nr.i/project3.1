package controller;

import common.service.RealCustomerService;
import model.entity.RealCustomer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Hi on 8/11/2020.
 */
@WebServlet("/showingcustomernumber.do")
public class CustomerNumberForPerson extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            RealCustomerService realCustomerService = new RealCustomerService();
            req.setAttribute("customernumberforperson", realCustomerService.showCustomerNumber((Integer) req.getAttribute("id")));
            req.getRequestDispatcher("/showingcustomernumberforperson.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

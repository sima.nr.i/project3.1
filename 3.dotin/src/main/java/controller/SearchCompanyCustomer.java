package controller;

import model.entity.LegalCustomer;
import common.service.LegalCustomerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hi on 8/16/2020.
 */ @WebServlet("/searchcompanycustomer.do")
public class SearchCompanyCustomer extends HttpServlet{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            LegalCustomer legalCustomer;
            if (req.getParameter("id")==""||req.getParameter("id")==null)
            {
                legalCustomer =new LegalCustomer(-1,req.getParameter("companyname"),
                        req.getParameter("economicid"));
            }
            else
            legalCustomer =new LegalCustomer(Integer.parseInt(req.getParameter("id")),req.getParameter("companyname"),
                    req.getParameter("economicid"));
            LegalCustomerService legalCustomerService = new LegalCustomerService();
            List<LegalCustomer> legalCustomers =new ArrayList<LegalCustomer>();
            legalCustomers = legalCustomerService.search(legalCustomer);
            req.setAttribute("list2", legalCustomerService.search(legalCustomer));
            req.getRequestDispatcher("/managing_companycustomer.jsp").forward(req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

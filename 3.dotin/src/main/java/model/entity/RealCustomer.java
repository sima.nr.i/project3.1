package model.entity;

import com.sun.istack.internal.NotNull;
import common.MyGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Hi on 8/1/2020.
 */
@Entity(name = "realcustomer")
@Table(name = "REALCUSTOMER")
public class RealCustomer implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Column(columnDefinition = "VARCHAR(20)",unique = true,nullable = false)
    @GeneratedValue(generator = MyGenerator.generatorName)
    @GenericGenerator(name = MyGenerator.generatorName, strategy = "a.b.c.MyGenerator")
    private String customerNumber;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String name;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String lastName;
    @Column(columnDefinition = "VARCHAR(20)",unique = true,nullable = false)
    @NotNull
    private String identityNumber;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String nameOfFather;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String  dateOfBirth;

    public RealCustomer(int id, String customerNumber, String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.id = id;
        this.customerNumber = customerNumber;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    //==================================================================================================================================
    public RealCustomer(String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public RealCustomer(int id, String name, String lastName, String identityNumber, String nameOfFather, String dateOfBirth) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
        this.nameOfFather = nameOfFather;
        this.dateOfBirth = dateOfBirth;
    }

    public RealCustomer(int id, String name, String lastName, String identityNumber) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.identityNumber = identityNumber;
    }

    public RealCustomer() {
    }

    //=====================================================================================================================================
    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfFather() {
        return nameOfFather;
    }

    public void setNameOfFather(String nameOfFather) {
        this.nameOfFather = nameOfFather;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
}

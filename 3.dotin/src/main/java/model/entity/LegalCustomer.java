package model.entity;

import com.sun.istack.internal.NotNull;
import common.MyGenerator;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;
import javax.persistence.*;
import java.io.Serializable;
/**
 * Created by Hi on 8/1/2020.
 */
@Entity(name ="legalcustomer")
@javax.persistence.Table(name="LEGALCUSTOMER")

public class LegalCustomer implements Serializable {
    @Id
    @Column(columnDefinition = "NUMBER",nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    @Column(columnDefinition = "VARCHAR(20)",unique = true,nullable = false)
    private String customerNumber;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String companyName;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String economicId;
    @Column(columnDefinition = "VARCHAR(20)",nullable = false)
    private String dateOfRegistering;
    public LegalCustomer(int id, String customernumber, String companyname, String economicid, String dateofregistering) {
        this.id = id;
        this.customerNumber = customernumber;
        this.companyName = companyname;
        this.economicId = economicid;
        this.dateOfRegistering = dateofregistering;
    }

    public LegalCustomer(String customerNumber, String companyName, String economicId, String dateOfRegistering) {
        this.customerNumber = customerNumber;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    //================================================================================================================================
    public LegalCustomer(String companyName, String economicId, String dateOfRegistering) {
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public LegalCustomer(int id, String companyName, String economicId, String dateOfRegistering) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
        this.dateOfRegistering = dateOfRegistering;
    }

    public LegalCustomer(int id, String companyName, String economicId) {
        this.id = id;
        this.companyName = companyName;
        this.economicId = economicId;
    }

    public LegalCustomer() {
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEconomicId() {
        return economicId;
    }

    public void setEconomicId(String economicId) {
        this.economicId = economicId;
    }

    public String getDateOfRegistering() {
        return dateOfRegistering;
    }

    public void setDateOfRegistering(String dateOfRegistering) {
        this.dateOfRegistering = dateOfRegistering;
    }
}

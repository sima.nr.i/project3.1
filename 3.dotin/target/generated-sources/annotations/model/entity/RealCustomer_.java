package model.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(RealCustomer.class)
public abstract class RealCustomer_ {

	public static volatile SingularAttribute<RealCustomer, String> lastName;
	public static volatile SingularAttribute<RealCustomer, String> nameOfFather;
	public static volatile SingularAttribute<RealCustomer, String> identityNumber;
	public static volatile SingularAttribute<RealCustomer, String> name;
	public static volatile SingularAttribute<RealCustomer, String> dateOfBirth;
	public static volatile SingularAttribute<RealCustomer, Integer> id;
	public static volatile SingularAttribute<RealCustomer, String> customerNumber;

}

